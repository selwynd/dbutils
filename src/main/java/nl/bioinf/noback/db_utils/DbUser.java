package nl.bioinf.noback.db_utils;

public class DbUser{
	String host;
	String userName;
	String databaseName;
	String databasePassword;

	@Override
	public String toString() {
		return "User [host=" + host + ", userName=" + userName
				+ ", databaseName=" + databaseName + "]";
	}
	
	
	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}


	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * @param databaseName the databaseName to set
	 */
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}


	/**
	 * @param databasePassword the databasePassword to set
	 */
	public void setDatabasePassword(String databasePassword) {
		this.databasePassword = databasePassword;
	}


	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @return the databaseName
	 */
	public String getDatabaseName() {
		return databaseName;
	}
	/**
	 * @return the databasePassword
	 */
	public String getDatabasePassword() {
		return databasePassword;
	}
	
	
}