package nl.bioinf.noback.db_utils;

public class PasswordRetrievalException extends Exception {

	public PasswordRetrievalException(String arg0) {
		super(arg0);
	}

}
