# DB utils

For fetching DB credentials (when in classroom setting).

## Getting Started

Build and put jar in classpath.
Then access the credentials like this.

```java
import nl.bioinf.noback.db_utils.DbCredentials;
import nl.bioinf.noback.db_utils.DbUser;
class DBCredDemo{
    void useCredentials() {
        DbUser dbUser = DbCredentials.getMySQLuser();
        String user = dbUser.getUserName();
        String passWrd = dbUser.getDatabasePassword();
        String host = dbUser.getHost();
        String dbName = dbUser.getDatabaseName();
    }
}
```

### Prerequisites

A `.my.cnf` file in your home dir, with contents like this:

```
[client]
user=<db user name>
password=<db password>
host=localhost
database=<default database>
```

## Authors

* **Michiel Noback** - [Bioinformatics at Hanze University of Applied Science](https://bioinf.nl/~michiel)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

